package fi.vtt.training.kubeone.service.hello.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import fi.vtt.training.kubeone.data.ServerIdentity;
import fi.vtt.training.kubeone.data.TimestampMessage;
import fi.vtt.training.kubeone.service.hello.config.properties.TimestampServerProperties;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@SuppressWarnings("WeakerAccess")
@Service
public class TimestampRequestHandler {
    private static final Logger log = LoggerFactory.getLogger(TimestampRequestHandler.class);

    private OkHttpClient httpClient;
    private ObjectMapper objectMapper;
    private ServerIdentity serverIdentity;
    private TimestampServerProperties serverProperties;

    @Autowired
    public TimestampRequestHandler(TimestampServerProperties serverProperties, ObjectMapper objectMapper, ServerIdentity serverIdentity) {
        this.serverProperties = serverProperties;
        this.objectMapper = objectMapper;
        this.serverIdentity = serverIdentity;

        this.httpClient = new OkHttpClient.Builder().build();
    }

    public TimestampMessage fetchTimeStamp() {
        try {
            Request request = new Request.Builder()
                    .url(generateUrl())
                    .get()
                    .build();

            Response response = httpClient.newCall(request).execute();
            try (ResponseBody responseBody = response.body()) {
                if (response.isSuccessful()) {
                    if (responseBody != null) {
                        String body = responseBody.string();

                        return objectMapper.readValue(body, TimestampMessage.class);
                    }
                }
            }
        }
        catch (IOException e) {
            // We can't really do anything about it
        }

        log.warn("Can't get timestamp.");

        return null;
    }

    private HttpUrl generateUrl() {
        return new HttpUrl.Builder()
                .scheme("http")
                .host(serverProperties.getHostname())
                .port(serverProperties.getPort())
                .addPathSegment("timestamp")
                .addPathSegment(serverIdentity.getValue())
                .build();
    }
}
