package fi.vtt.training.kubeone.service.hello.resources;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason="Can't fetch timestamp.")
public class TimestampException extends RuntimeException {
}
