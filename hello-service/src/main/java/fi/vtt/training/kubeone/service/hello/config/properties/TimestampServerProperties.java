package fi.vtt.training.kubeone.service.hello.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("jobexample.timestamp")
@Component
public class TimestampServerProperties {
    /**
     * Server hostname.
     */
    private String hostname = "localhost";

    /**
     * Server port-number.
     */
    private int port = 8100;

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
