package fi.vtt.training.kubeone.service.hello.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("jobexample.api")
@Component
public class ApiProperties {
    /**
     * Whom to greet.
     */
    private String greet = "World";

    /**
     * The rate (percentage) to generate error exit code.
     */
    private int errorRate = 0;

    public String getGreet() {
        return greet;
    }

    public void setGreet(String greet) {
        this.greet = greet;
    }

    public int getErrorRate() {
        return errorRate;
    }

    public void setErrorRate(int errorRate) {
        this.errorRate = errorRate;
    }
}
