package fi.vtt.training.kubeone.service.hello.resources;

import fi.vtt.training.kubeone.data.ServerIdentity;
import fi.vtt.training.kubeone.data.TimestampMessage;
import fi.vtt.training.kubeone.error.ErrorRate;
import fi.vtt.training.kubeone.service.hello.config.properties.ApiProperties;
import fi.vtt.training.kubeone.data.HelloMessage;
import fi.vtt.training.kubeone.service.hello.util.TimestampRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloResource {
    private ErrorRate errorRate;
    private ApiProperties apiProperties;
    private ServerIdentity serverIdentity;
    private TimestampRequestHandler timestampRequestHandler;

    @Autowired
    public HelloResource(ErrorRate errorRate, ApiProperties apiProperties, ServerIdentity serverIdentity,
                         TimestampRequestHandler timestampRequestHandler) {
        this.errorRate = errorRate;
        this.apiProperties = apiProperties;
        this.serverIdentity = serverIdentity;
        this.timestampRequestHandler = timestampRequestHandler;
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    @ResponseBody
    public HelloMessage hello() {
        if (errorRate.isError()) {
            throw new ErrorRateException();
        }
        else {
            TimestampMessage timestamp = timestampRequestHandler.fetchTimeStamp();
            if (timestamp != null) {
                return new HelloMessage(
                        "Hello " + apiProperties.getGreet(),
                        timestamp,
                        apiProperties.getErrorRate(),
                        serverIdentity
                );
            }
            else {
                throw new TimestampException();
            }
        }
    }
}
