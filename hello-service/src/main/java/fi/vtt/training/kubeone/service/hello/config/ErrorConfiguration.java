package fi.vtt.training.kubeone.service.hello.config;

import fi.vtt.training.kubeone.error.ErrorRate;
import fi.vtt.training.kubeone.service.hello.config.properties.ApiProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ErrorConfiguration {
    private static final Logger log = LoggerFactory.getLogger(ErrorConfiguration.class);

    @Bean
    public ErrorRate errorRate(ApiProperties properties) {
        log.debug("Error rate is set to (%): {}", properties.getErrorRate());

        return new ErrorRate(properties.getErrorRate());
    }
}
