package fi.vtt.training.kubeone.service.timestamp.resources;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason="Random matched configured error-rate.")
public class ErrorRateException extends RuntimeException {
}
