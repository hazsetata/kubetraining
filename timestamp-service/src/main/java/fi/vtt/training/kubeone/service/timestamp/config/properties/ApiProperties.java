package fi.vtt.training.kubeone.service.timestamp.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("jobexample.api")
@Component
public class ApiProperties {
    /**
     * The rate (percentage) to generate error exit code.
     */
    private int errorRate = 0;

    public int getErrorRate() {
        return errorRate;
    }

    public void setErrorRate(int errorRate) {
        this.errorRate = errorRate;
    }
}
