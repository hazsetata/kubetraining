package fi.vtt.training.kubeone.service.timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@EnableConfigurationProperties
public class TimestampApplication {
    private static final Logger log = LoggerFactory.getLogger(TimestampApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        SpringApplication application = new SpringApplication(TimestampApplication.class);
        application.setBannerMode(Banner.Mode.CONSOLE);

        Environment env = application.run(args).getEnvironment();

        String serverPort = env.getProperty("server.port");
        String hostAddress = InetAddress.getLocalHost().getHostAddress();

        log.info(createAccessInformation(serverPort, hostAddress));
    }

    private static String createAccessInformation(String serverPort, String hostAddress) {
        StringBuilder retValue = new StringBuilder();

        retValue.append("Server access information\n");
        retValue.append("       Local:  http://127.0.0.1:").append(serverPort).append("\n");
        retValue.append("       Remote: http://").append(hostAddress).append(":").append(serverPort).append("\n");

        return retValue.toString();
    }
}
