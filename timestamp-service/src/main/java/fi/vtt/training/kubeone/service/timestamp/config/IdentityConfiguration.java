package fi.vtt.training.kubeone.service.timestamp.config;

import fi.vtt.training.kubeone.data.ServerIdentity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IdentityConfiguration {
    @Bean
    public ServerIdentity serverIdentity() {
        return new ServerIdentity();
    }
}
