package fi.vtt.training.kubeone.service.timestamp.resources;

import fi.vtt.training.kubeone.data.ServerIdentity;
import fi.vtt.training.kubeone.data.TimestampMessage;
import fi.vtt.training.kubeone.error.ErrorRate;
import fi.vtt.training.kubeone.service.timestamp.config.properties.ApiProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;

@RestController
public class TimestampResource {
    private static final Logger log = LoggerFactory.getLogger(TimestampResource.class);

    private ErrorRate errorRate;
    private ApiProperties apiProperties;
    private ServerIdentity serverIdentity;

    @Autowired
    public TimestampResource(ErrorRate errorRate, ApiProperties apiProperties, ServerIdentity serverIdentity) {
        this.errorRate = errorRate;
        this.apiProperties = apiProperties;
        this.serverIdentity = serverIdentity;
    }

    @RequestMapping(value = "/timestamp/{helloServerId}", method = RequestMethod.GET)
    @ResponseBody
    public TimestampMessage timestamp(@PathVariable String helloServerId) {
        log.debug("Request from server: {}", helloServerId);

        if (errorRate.isError()) {
            throw new ErrorRateException();
        }
        else {
            return new TimestampMessage(
                    ZonedDateTime.now(),
                    apiProperties.getErrorRate(),
                    serverIdentity
            );
        }
    }
}
