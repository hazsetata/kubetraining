buildscript {
    ext {
        logbackLoglevel='DEBUG'

        okHttpVersion='4.1.0'
        jsonPathVersion='2.4.0'
        picoCliVersion='4.0.4'

        slf4jVersion='1.7.28'
        logbackVersion='1.2.3'

        jUnitVersion='5.5.1'
        hamcrestVersion='1.3'
        mockitoVersion='2.23.0'

        springBootVersion='2.1.8.RELEASE'
        springDependencyManagementVersion='1.0.8.RELEASE'
        gitPropertiesVersion='2.2.0'
    }
    repositories {
        jcenter()
    }
}

plugins {
    id 'base'
    id 'jacoco'
    id 'org.springframework.boot' version "$springBootVersion" apply false
    id 'io.spring.dependency-management' version "$springDependencyManagementVersion" apply false
}

ext {
    versionMajor = 1
    versionMinor = 1
    versionPoint = 0
}

allprojects {
    group 'fi.vtt.training.kubeone'
    version computeVersion()

    buildscript {
        repositories {
            jcenter()
            maven {
                url "https://plugins.gradle.org/m2/"
            }
        }
    }

    repositories {
        jcenter()
    }
}

def javaLibraryProjects() {
    subprojects.findAll { subproject ->
        subproject.name.endsWith('-lib')
    }
}

def javaProjects() {
    subprojects.findAll { subproject ->
        !subproject.name.endsWith('-lib')
    }
}

configure(javaLibraryProjects()) {
    apply plugin: 'java-library'
    apply plugin: 'maven-publish'
    apply plugin: "jacoco"

    sourceCompatibility = 1.8
    targetCompatibility = 1.8

    test {
        testLogging {
            exceptionFormat = 'full'
        }
    }

    jacocoTestReport {
        reports {
            xml.enabled false
            csv.enabled false
            html.enabled true
        }

        afterEvaluate {
            getClassDirectories().setFrom(files(classDirectories.files.collect {
                fileTree(dir: it,
                        exclude: ['fi/vtt/training/kubeone/example/**'])
            }))
        }
    }

    task sourcesJar(type: Jar) {
        from sourceSets.main.allJava
        archiveClassifier = 'sources'
    }

    task javadocJar(type: Jar) {
        from javadoc
        archiveClassifier = 'javadoc'
    }

    publishing {
        publications {
            mavenJava(MavenPublication) {
                from components.java
                artifact sourcesJar
                artifact javadocJar
                versionMapping {
                    usage('java-api') {
                        fromResolutionOf('runtimeClasspath')
                    }
                    usage('java-runtime') {
                        fromResolutionResult()
                    }
                }
                pom {
                    name = 'VTT Training - Kubernetes'
                    description = 'Small components usd for Kubernetes training.'
                    url = 'https://gitlab.com/hazsetata/kubetraining'
                    developers {
                        developer {
                            id = 'hazsetata'
                            name = 'Zsolt Homorodi'
                            email = 'zsolt.homorodi@vtt.fi'
                        }
                    }
                    scm {
                        connection = 'scm:git:git@gitlab.com:hazsetata/kubetraining.git'
                        developerConnection = 'scm:git:git@gitlab.com:hazsetata/kubetraining.git'
                        url = 'https://gitlab.com/hazsetata/kubetraining'
                    }
                }
            }
        }
        repositories {
            maven {
                url "$rootProject.buildDir/repository"
            }
        }
    }

    task allDeps(type: DependencyReportTask) {}
}

configure(javaProjects()) {
    apply plugin: 'java'
    apply plugin: "jacoco"

    sourceCompatibility = 1.8
    targetCompatibility = 1.8

    test {
        testLogging {
            exceptionFormat = 'full'
        }
    }

    compileJava.dependsOn(processResources)

    processResources {
        filesMatching('**/logback.xml') {
            filter {
                it.replace('${logback.loglevel}', logbackLoglevel)
            }
        }
        filesMatching('**/*.properties') { expand(project.properties) }
    }

    jacocoTestReport {
        reports {
            xml.enabled false
            csv.enabled false
            html.enabled true
        }

        afterEvaluate {
            getClassDirectories().setFrom(files(classDirectories.files.collect {
                fileTree(dir: it,
                        exclude: ['fi/vtt/training/kubeone/example/**'])
            }))
        }
    }

    task allDeps(type: DependencyReportTask) {}
}

task codeCoverageReport(type: JacocoReport) {
    getAdditionalSourceDirs().setFrom(files(javaLibraryProjects().sourceSets.main.allSource.srcDirs))
    getSourceDirectories().setFrom(files(javaLibraryProjects().sourceSets.main.allSource.srcDirs))
    getClassDirectories().setFrom(files(javaLibraryProjects().sourceSets.main.output))
    getExecutionData().setFrom(files(javaLibraryProjects().jacocoTestReport.executionData))
    reports {
        html.enabled = true
        html.destination = new File("${buildDir}/reports/jacoco")
        xml.enabled = true
        csv.enabled = false
    }
    onlyIf = {
        true
    }
    doFirst {
        getExecutionData().setFrom(files(executionData.findAll {
            it.exists()
        }))
    }
    afterEvaluate {
        getClassDirectories().setFrom(files(classDirectories.files.collect {
            fileTree(dir: it,
                    exclude: ['fi/vtt/training/kubeone/example/**'])
        }))
    }
}

task testReport(type: TestReport) {
    destinationDir = file("$buildDir/reports/allTests")
    // Include the results from the 'test' task in all subprojects
    reportOn subprojects.collect { it.tasks.withType(Test) }
}

task versionTxt() {
    doLast {
        file("$projectDir/version.txt").text = String.format('%d.%d.%d', versionMajor, versionMinor, versionPoint)
    }
}

def computeVersion() {
    // Version format: <major>.<minor>.<point>.<build>
    return String.format('%d.%d.%d.%s', versionMajor, versionMinor, versionPoint, getBuildNumber())
}

static def getBuildNumber() {
    if (System.env.BUILD_NUMBER)
        return System.env.BUILD_NUMBER
    else if (System.env.BITBUCKET_BUILD_NUMBER)
        return System.env.BITBUCKET_BUILD_NUMBER
    else if (System.env.CI_PIPELINE_IID)
        return System.env.CI_PIPELINE_IID

    def proc = "git rev-parse --short HEAD".execute()
    proc.waitFor()

    return "DEV-" + (proc.exitValue() ? "UNKNOWN" : proc.text.trim())
}

static def getDate() {
    def date = new Date()
    def formattedDate = date.format('HH:mm MMMM d, YYYY')
    return formattedDate
}
