package fi.vtt.training.kubeone.error;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("WeakerAccess")
public class ErrorRateTest {
    @Test
    public void testZeroWorks() {
        ErrorRate errorRate = new ErrorRate(0);
        assertFalse(errorRate.isError());
    }

    @Test
    public void testBellowZeroWorks() {
        ErrorRate errorRate = new ErrorRate(-1);
        assertFalse(errorRate.isError());
    }

    @Test
    public void testHundredWorks() {
        ErrorRate errorRate = new ErrorRate(100);
        assertTrue(errorRate.isError());
    }

    @Test
    public void testOverHundredWorks() {
        ErrorRate errorRate = new ErrorRate(200);
        assertTrue(errorRate.isError());
    }

    @Test
    public void testTwentyWorks() {
        ErrorRate errorRate = new ErrorRate(20);
        int errorCount = 0;
        for (int i=0; i < 1000; i++) {
            if (errorRate.isError()) {
                errorCount++;
            }
        }

        assertNotEquals(errorCount, 0);
        assertTrue(errorCount >= 100);
        assertTrue(errorCount <= 300);
    }
}