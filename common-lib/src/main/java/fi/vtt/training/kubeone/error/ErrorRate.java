package fi.vtt.training.kubeone.error;

import java.util.Random;

public class ErrorRate {
    private final int errorPercentage;
    private final Random random;

    public ErrorRate(int errorPercentage) {
        this.errorPercentage = errorPercentage;
        this.random = new Random();
    }

    public boolean isError() {
        if (errorPercentage <= 0) {
            return false;
        }
        else if (errorPercentage >= 100) {
            return true;
        }
        else {
            return (random.nextInt(100) < errorPercentage);
        }
    }
}
