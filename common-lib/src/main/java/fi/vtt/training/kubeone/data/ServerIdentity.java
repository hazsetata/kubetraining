package fi.vtt.training.kubeone.data;

import java.util.UUID;

public class ServerIdentity {
    private final String serverIdentity;

    public ServerIdentity() {
        serverIdentity = UUID.randomUUID().toString();
    }

    public String getValue() {
        return serverIdentity;
    }
}
