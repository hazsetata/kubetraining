package fi.vtt.training.kubeone.data;

import java.time.ZonedDateTime;

public class TimestampMessage {
    private ZonedDateTime timeStamp;
    private int errorRate;
    private ServerIdentity serverIdentity;

    public TimestampMessage() {
    }

    public TimestampMessage(ZonedDateTime timeStamp, int errorRate, ServerIdentity serverIdentity) {
        this.timeStamp = timeStamp;
        this.errorRate = errorRate;
        this.serverIdentity = serverIdentity;
    }

    public ZonedDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(ZonedDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getErrorRate() {
        return errorRate;
    }

    public void setErrorRate(int errorRate) {
        this.errorRate = errorRate;
    }

    public ServerIdentity getServerIdentity() {
        return serverIdentity;
    }

    public void setServerIdentity(ServerIdentity serverIdentity) {
        this.serverIdentity = serverIdentity;
    }
}
