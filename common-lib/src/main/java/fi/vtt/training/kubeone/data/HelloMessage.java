package fi.vtt.training.kubeone.data;

public class HelloMessage {
    private String greeting;
    private TimestampMessage timeStamp;
    private int errorRate;
    private ServerIdentity serverIdentity;

    public HelloMessage() {
    }

    public HelloMessage(String greeting, TimestampMessage timeStamp, int errorRate, ServerIdentity serverIdentity) {
        this.greeting = greeting;
        this.timeStamp = timeStamp;
        this.errorRate = errorRate;
        this.serverIdentity = serverIdentity;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public TimestampMessage getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(TimestampMessage timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getErrorRate() {
        return errorRate;
    }

    public void setErrorRate(int errorRate) {
        this.errorRate = errorRate;
    }

    public ServerIdentity getServerIdentity() {
        return serverIdentity;
    }

    public void setServerIdentity(ServerIdentity serverIdentity) {
        this.serverIdentity = serverIdentity;
    }
}
