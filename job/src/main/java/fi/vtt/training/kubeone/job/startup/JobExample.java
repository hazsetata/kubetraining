package fi.vtt.training.kubeone.job.startup;

import fi.vtt.training.kubeone.error.ErrorRate;
import fi.vtt.training.kubeone.job.config.ApiProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.time.ZonedDateTime;
import java.util.concurrent.Callable;

/**
 * @author Zsolt Homorodi.
 */
@Component
@CommandLine.Command(
        name = "job-example",
        description = "Example Hello-World like Job",
        footer = {
                "",
                "Configuration keys must be specified as environment variables, " +
                        "please check the README."
        },
        mixinStandardHelpOptions = true,
        version = "job-example - v0.1"
)
public class JobExample implements CommandLineRunner, Callable<Integer> {
    private static final Logger log = LoggerFactory.getLogger(JobExample.class);

    private ApiProperties apiProperties;
    private ErrorRate errorRate;

    @Autowired
    public JobExample(
            ApiProperties apiProperties
    ) {
        this.apiProperties = apiProperties;
        this.errorRate = new ErrorRate(apiProperties.getErrorRate());
    }

    @Override
    public void run(String... args) throws CommandErrorException {
        CommandLine cmd = new CommandLine(this);
        int exitCode = cmd.execute(args);

        if (exitCode != 0) {
            throw new CommandErrorException(exitCode);
        }
    }

    @Override
    public Integer call() throws Exception {
        log.info("Hello {}", apiProperties.getGreet());
        log.info("Timestamp: {}", ZonedDateTime.now());
        log.info("Configured error rate: {} %", apiProperties.getErrorRate());

        if (errorRate.isError()) {
            return 123;
        }
        else {
            return 0;
        }
    }
}
