package fi.vtt.training.kubeone.job.startup;

@SuppressWarnings("WeakerAccess")
public class CommandErrorException extends Exception{
    private final int errorCode;

    public CommandErrorException(int errorCode) {
        super("Command exited with code: " + errorCode);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
