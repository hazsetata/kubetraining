package fi.vtt.training.kubeone.job;

import fi.vtt.training.kubeone.job.startup.CommandErrorException;
import org.springframework.boot.ExitCodeExceptionMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Zsolt Homorodi.
 */
@SpringBootApplication
@ComponentScan(basePackages="fi.vtt.training.kubeone")
@EnableConfigurationProperties
public class JobExampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(JobExampleApplication.class, args);
    }

    @Bean
    ExitCodeExceptionMapper exitCodeToExceptionMapper() {
        return exception -> {
            if (exception.getCause() instanceof CommandErrorException) {
                return ((CommandErrorException)exception.getCause()).getErrorCode();
            }

            return 1;
        };
    }
}
